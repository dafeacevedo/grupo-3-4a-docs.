from rest_framework import serializers
from hconocimientoapp.models.user import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'name', 'email',
                  'identificacion', 'telefono', 'profesion', 'fecha_nacimiento']
