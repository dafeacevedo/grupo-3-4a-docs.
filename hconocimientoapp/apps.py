from django.apps import AppConfig


class HconocimientoappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hconocimientoapp'
